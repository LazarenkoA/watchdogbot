module watchdog

// +heroku goVersion go1.15
go 1.15

require (
	github.com/clintjedwards/avail v1.0.1
	github.com/garyburd/redigo v1.6.2
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/matryer/resync v0.0.0-20161211202428-d39c09a11215
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
